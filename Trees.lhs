 module Trees where

-- This code was automatically extracted from a .lhs file that
-- uses the following convention:

-- lines beginning with ">" are executable
-- lines beginning with "<" are in the text,
     but not necessarily executable
-- lines beginning with "|" are also in the text,
     but are often just expressions or code fragments.

 data Shape = Rectangle Side Side
            | Ellipse Radius Radius
            | RtTriangle Side Side
            | Polygon [Vertex]
      deriving Show

 type Radius = Float
 type Side   = Float
 type Vertex  = (Float,Float)

 data List a = Nil | MkList a (List a)

 Nil    :: List a
 MkList :: a -> List a -> List a

 []  :: [a]
 (:) :: a -> [a] -> [a]

 data Tree a = Leaf a | Branch (Tree a) (Tree a)

 data IntegerTree = Leaf Integer | Branch IntegerTree IntegerTree
 data SimpleTree  = Leaf | Branch SimpleTree SimpleTree

 data InternalTree a = ILeaf 
                     | IBranch a (InternalTree a) (InternalTree a)

 data FancyTree a b = FLeaf a 
                    | FBranch b (FancyTree a b) (FancyTree a b)

 mapTree :: (a->b) -> Tree a -> Tree b

 mapTree f (Leaf x)       = Leaf (f x)
 mapTree f (Branch t1 t2) = Branch (mapTree f t1) 
                                    (mapTree f t2)

 fringe               :: Tree a -> [a]
 fringe (Leaf x)       = [x]
 fringe (Branch t1 t2) = fringe t1 ++ fringe t2

 treeSize               :: Tree a -> Integer
 treeSize (Leaf _)       = 1
 treeSize (Branch t1 t2) = treeSize t1 + treeSize t2

 treeHeight		   :: Tree a -> Integer
 treeHeight (Leaf _)       = 0
 treeHeight (Branch t1 t2) = 1 + max (treeHeight t1) 
                                     (treeHeight t2)

 data Expr = C Float | Add Expr Expr | Sub Expr Expr
           | Mul Expr Expr | Div Expr Expr

 data Expr = C Float | Expr :+ Expr | Expr :- Expr
           | Expr :* Expr | Expr :/ Expr 
           | V [Char]
           | Let String Expr Expr      
      deriving Show


-- >>> (C 10.0 :+ (C 8 :/ C 2)) :* (C 7 :- C 4)
-- (C 10.0 :+ (C 8.0 :/ C 2.0)) :* (C 7.0 :- C 4.0)
--

 subst :: String -> Expr -> Expr -> Expr

 subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
 subst v0 e0 (C c)          = (C c)
 subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
 subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
 subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
 subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
 subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

 evaluate :: Expr -> Float

 evaluate (C x) = x
 evaluate (e1 :+ e2)    = evaluate e1 + evaluate e2
 evaluate (e1 :- e2)    = evaluate e1 - evaluate e2
 evaluate (e1 :* e2)    = evaluate e1 * evaluate e2
 evaluate (e1 :/ e2)    = evaluate e1 / evaluate e2
 evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
 evaluate (V v)         = 0.0   

 data InternalTree a = ILeaf 
                     | IBranch a (InternalTree a) (InternalTree a)

 takeTree      :: Int -> InternalTree a -> InternalTree a
 takeTreeWhile :: (a -> Bool) -> InternalTree a -> InternalTree a

 t = let t' = IBranch 1 ILeaf ILeaf
     in Branch 2 t' t'

test0 = Let "x" (C 5) (Let "y" (C 3) (V "x" :- V "y"))
test1 = (C 5) :+ (C 7) :- (C 2)

foldExpr (kali, bagi, tamb, kura, le, ve, con) (C c)    = con c
foldExpr (kali, bagi, tamb, kura, le, ve, con) (e1 :+ e2) = tamb (foldExpr (kali, bagi, tamb, kura, le, ve, con) e1) (foldExpr (kali, bagi, tamb, kura, le, ve, con) e2)
foldExpr (kali, bagi, tamb, kura, le, ve, con) (e1 :- e2) = kura (foldExpr (kali, bagi, tamb, kura, le, ve, con) e1) (foldExpr (kali, bagi, tamb, kura, le, ve, con) e2)
foldExpr (kali, bagi, tamb, kura, le, ve, con) (e1 :* e2) = kali (foldExpr (kali, bagi, tamb, kura, le, ve, con) e1) (foldExpr (kali, bagi, tamb, kura, le, ve, con) e2)
foldExpr (kali, bagi, tamb, kura, le, ve, con) (e1 :/ e2) = tamb (foldExpr (kali, bagi, tamb, kura, le, ve, con) e1) (foldExpr (kali, bagi, tamb, kura, le, ve, con) e2)
foldExpr (kali, bagi, tamb, kura, le, ve, con) (Let v e0 e1) = le v e0 e1
foldExpr (kali, bagi, tamb, kura, le, ve, con) (V v) = ve v

newEvaluate = foldExpr (kali, bagi, tamb, kura, le, ve, con)
    where
        kali = (*)
        bagi = (/)
        tamb = (+)
        kura = (-)
        le v e0 e1 = newEvaluate (subst v e0 e1)
        ve v = 0.0
        con c = c

countConstant = foldExpr (kali, bagi, tamb, kura, le, ve, con)
    where
        kali = (+)
        bagi = (+)
        tamb = (+)
        kura = (+)
        le v e0 e1 = countConstant (subst v e0 e1)
        ve v = 0.0
        con _ = 1

countVariable = foldExpr (kali, bagi, tamb, kura, le, ve, con)
    where
        kali = (+)
        bagi = (+)
        tamb = (+)
        kura = (+)
        le v e0 e1 = 1 + countVariable (subst v e0 e1)
        ve v = 0.0
        con _ = 0.0

countOperators = foldExpr (kali, bagi, tamb, kura, le, ve, con)
    where
        kali = (\x y -> x + y + 1)
        bagi = (\x y -> x + y + 1)
        tamb = (\x y -> x + y + 1)
        kura = (\x y -> x + y + 1)
        le v e0 e1 = (countOperators e1) + 1
        ve v = 0
        con _ = 0       
                        
data EvaluationErrorReason = DivByZero | NegativeSquareRoot
                        deriving (Show)
data CheckedEvaluation evaluationType = EvaluationOk evaluationType | EvaluationError EvaluationErrorReason
                        deriving (Show)

instance Functor CheckedEvaluation where
    fmap f (EvaluationError reason) = (EvaluationError reason)
    fmap f (EvaluationOk x) = EvaluationOk (f x)

instance Applicative CheckedEvaluation where
    pure f = EvaluationOk f
    (EvaluationError reason) <*> _ = (EvaluationError reason)
    (EvaluationOk f) <*> something = fmap f something

checkedDivision (EvaluationOk x) (EvaluationOk y)
    | y == 0.0  = EvaluationError DivByZero
    | otherwise = EvaluationOk (x / y)
checkedDivision (EvaluationError reason) _ = (EvaluationError reason)

evaluateAndCheck =  foldExpr (fconst,fadd,fsub,fmul,fdiv,fvar,flet)
                    where
                        fconst c = EvaluationOk c
                        fadd x y = (+) <$> x <*> y
                        fsub x y = (-) <$> x <*> y
                        fmul x y = (*) <$> x <*> y
                        fdiv = checkedDivision
                        fvar v = EvaluationOk 0.0
                        flet var expr1 expr2 = evaluateAndCheck (substitute var expr1 expr2)

isDivisionByZero expr =
    case evaluateAndCheck expr of
        (EvaluationOk _) -> False
        (EvaluationError DivByZero) -> True
        (EvaluationError _) -> False
